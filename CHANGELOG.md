This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Accounting Analytics Backend Connector for CouchBase


## [v2.0.0-SNAPSHOT] [r5.0.0] - 

- Switched JSON management to gcube-jackson [#19115]
- Fixed distro files and pom according to new release procedure


## [v1.7.0] [r4.10.0] - 2018-02-15

- Added the possibility to specify the context to query [#10188]


## [v1.6.0] [r4.8.0] - 2017-12-06

- Created uber-jar instead of jar-with-dependencies [#10139]
- Allowing to specify a limit when querying values for suggestion [#9943]


## [v1.5.0] [r4.7.0] - 2017-10-09

- Changed the way to get the target bucket [#9629]


## [v1.4.0] [r4.5.0] - 2017-06-07

- Added API to sum StorageUsage values got from MapReduce avoiding the client to perform such a computation


## [v1.3.0] [r4.3.0] - 2017-03-16

- Fixed bug on context based results and filters [#6747]
- Top are calculate by using Map-Reduce before trying to use N1QL query [#6175]


## [v1.2.0] [r4.2.0] - 2016-12-15

- Added method to get results for multiple context [#5857]
- Changed API to get designId
- Added API to support quota service


## [v1.1.0] [r4.1.0] - 2016-11-07

- Fixed bug on daily top request [#4956]
- Modified cluster connection timing
- Added API for calculate a Top without limit


## [v1.0.0] [r4.0.0] - 2016-07-28

- First Release

